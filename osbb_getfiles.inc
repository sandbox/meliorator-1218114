<?php
function osbb_debcred_file($p_type=0,$p_enc=0)
{
    $iCounterRows=1;
    if(!empty($_SESSION['debcred_report']))
        $sql=$_SESSION['debcred_report'];
    else 
        return;
        
    $sResult='';
    $result = db_query($sql);
    while ($oRow=db_fetch_array($result)) {
         $stmpdt = date('d.m.Y',strtotime($oRow['date_oper']));
         $note = (empty($oRow['note'])?'':'('.$oRow['note'].')');
         if($p_enc==0)
             $resn= iconv("utf-8", "windows-1251",$oRow['reason'].$note);
         else
             $resn= $oRow['reason'].$note;
         if($p_type!=0)
             $sResult .= $iCounterRows.';'.$stmpdt.';'.$oRow['sum'].';'.$resn.';'."\r\n";
         else 
             $sResult .= $iCounterRows.'.'."\t".$stmpdt."\t".$oRow['sum']."\t".$resn."\r\n";
         $iCounterRows++;
         $iTotalSum += $oRow['sum'];
    }
    if($p_enc==0)
        $sResult .= iconv("utf-8", "windows-1251", osbb_print_label(65).": $iTotalSum"."\r\n");
     else
       $sResult .= osbb_print_label(65).": $iTotalSum"."\r\n";
       
    return $sResult;
}
function osbb_communalka_file($p_type=0,$p_enc=0,$is_moder=false)
{
    $iCounterRows=1;
    if(!empty($_SESSION['comun_report']))
        $sql=$_SESSION['comun_report'];
    else 
        return;
        
    $iTotalPaidSum=0;
    $iTotalCreditSum=0;
    $iTotalDebetSum=0;
    if($p_type!=0)
        $sResult='N ;'.osbb_print_label(66).';'.osbb_print_label(67).';'.osbb_print_label(68).';'.osbb_print_label(69).';'.osbb_print_label(70).';'.osbb_print_label(71).';'.osbb_print_label(72)."\r\n";
    else
        $sResult='N '."\t".osbb_print_label(66).'             '."\t".osbb_print_label(67)."\t".osbb_print_label(68)."\t".osbb_print_label(69)."\t".osbb_print_label(70)."\t".osbb_print_label(71)."\t".osbb_print_label(72)."\r\n";
        
    if($p_enc==0)
        $sResult= iconv("utf-8", "windows-1251",$sResult);
        
    $result = db_query($sql);
     while ($oRow=db_fetch_array($result)) {
         
         $tmp = ($is_moder?$oRow['short_fio']:osbb_print_label(73));
         $tmp = trim($tmp);
         
         if($p_enc==0)
             $fio= iconv("utf-8", "windows-1251",$tmp);
         else
             $fio= $tmp;
         $fio = str_pad($fio,20,' ',STR_PAD_RIGHT);
         
         $oCalulated = osbb_calc_summ($oRow);
         $ppaid = $oCalulated->ppaid;
         $pdebet = $oCalulated->pdebt;
         $pcred = $oCalulated->pcredit;
             
         if($p_type!=0)
         {
             $sResult .= $oRow['apartment'].";$fio;";
             $sResult .= $oRow['mpaid'].';'.$ppaid.';'. $oRow['mcred'].';'.$pcred.';';
             $sResult .= $oRow['mdebet'].';'.$pdebet.';'."\r\n";
         }
         else
         {
             $sResult .= $oRow['apartment']."\t".$fio."\t";
             $sResult .= $oRow['mpaid']."\t".$ppaid."\t".$oRow['mcred']."\t".$pcred."\t";
             $sResult .= $oRow['mdebet']."\t".$pdebet."\t"."\r\n";
         } 
              
         $iCounterRows++;
         $iTotalPaidSum += $ppaid;
         $iTotalCreditSum += $pcred;
         $iTotalDebetSum += $pdebet;
    }
    if($p_enc==0)
        $sResult .= iconv("utf-8", "windows-1251", osbb_print_label(74).": $iTotalPaidSum ".osbb_print_label(40).": $iTotalCreditSum ".osbb_print_label(75).": $iTotalDebetSum"."\r\n");
     else
       $sResult .= osbb_print_label(74).": $iTotalPaidSum ".osbb_print_label(40).": $iTotalCreditSum ".osbb_print_label(75)." $iTotalDebetSum"."\r\n";
       
    return $sResult;
}