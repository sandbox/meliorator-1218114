<div id="sttabs">
	<ul>
		<li><a href="#sttabs-3"><?php print osbb_print_label(32) ?></a></li>
		<li><a href="#sttabs-2"><?php print osbb_print_label(16) ?></a></li>
		<li><a href="#sttabs-1"><?php print osbb_print_label(33) ?></a></li>
		<li><a href="#sttabs-4"><?php print osbb_print_label(31) ?></a></li>
		<li><a href="#sttabs-5"><?php print osbb_print_label(34) ?></a></li>
		<li><a href="#sttabs-6"><?php print osbb_print_label(35) ?></a></li>
	</ul>
	<div id="sttabs-1">
		 <table id="settings_owners_table"></table> 
		<div id="settings_owners_pager"></div>
	</div>
	<div id="sttabs-2">
		<table id="settings_group_table"></table> 
		<div id="settings_group_pager"></div>
	</div>
	<div id="sttabs-3">
		<table id="settings_types_table"></table> 
		<div id="settings_types_pager"></div>
	</div>
	<div id="sttabs-4">
		<table id="settings_premise_table"></table> 
		<div id="settings_premise_pager"></div>
	</div>
	<div id="sttabs-5">
		<table id="settings_periodic_table"></table> 
		<div id="settings_periodic_pager"></div>
	</div>
	<div id="sttabs-6">
		<table class='parmcontainer1' cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td>
					<label for="payon" ><?php print osbb_print_label(36) ?></label>
				</td>
				<td>
					<input type="text" id="payon" value="<?php print osbb_get_value('osbbpayon'); ?>" />
				</td>
			</tr>
			<tr>
				<td>
					<label for="comperiodname" ><?php print osbb_print_label(37) ?></label>
				</td>
				<td>
					<select id="comperiodname">
						<?php $ID=osbb_get_value('comunal_periodic_id'); print osbb_get_periodic_combobox($ID); ?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<label for="dtpStartDate" ><?php print osbb_print_label(98) ?></label>
				</td>
				<td>
					<input type="text" id="dtpStartDate" />
				</td>
			</tr>
		</table>
		<button  type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style='margin-top: 10px;' onclick="SaveOSBBSettings()">
			<span class="ui-button-text">
		        <?php print osbb_print_label(38) ?>
		    </span>
		</button>
	</div>
	<!--<button onclick="ShowConfig()">get xml config</button>
--></div>
<script type="text/javascript">
<?php
    //print ("var osbb_url_prefix='".osbb_url_prefix()."';");
    $sStartDate = osbb_get_value('credit_start_view_period');
    if(!empty($sStartDate)|| $sStartDate!=0)
    {
        $aTmpDate = explode('-',$sStartDate);
        $sTmpMonth = $aTmpDate[1]-1;
        $sDate = $aTmpDate[0].','.$sTmpMonth.','.$aTmpDate[2];
    }
    else
    {
        $aStartDate = osbb_start_date(2);
        $sScriptMonth = $aStartDate['month']-1;
        $sDate=$aStartDate['year'].','.$sScriptMonth.','.$aStartDate['day'];
    }
    
    print("$('#dtpStartDate').datepicker({dateFormat: 'dd.mm.yy'});");
    print ('$("#dtpStartDate").datepicker( "setDate",new Date('.$sDate.') );');
?>
</script>
<?php 