<div id="atabs">
	<ul>
		<li><a href="#atabs-1"><?php print osbb_print_label(45) ?></a></li>
		<li><a href="#atabs-2"><?php print osbb_print_label(46) ?></a></li>
		<li><a href="#atabs-3"><?php print osbb_print_label(47) ?></a></li>
	</ul>
	<div id="atabs-1">
		<fieldset>
        	<legend><?php print osbb_print_label(48) ?></legend>
        	<div class="month_cal month_cal_from">
        		<fieldset><legend><?php print osbb_print_label(49) ?></legend>
        			<div id="fromDate"></div>
        		</fieldset>
        	</div>
        	<div class="month_cal month_cal_to">
        		<fieldset><legend><?php print osbb_print_label(50) ?></legend>
        			<div id="toDate"></div>
        		</fieldset>
        	</div>
        	<div style="float:none;clear:both;">&nbsp;</div>
        	<div id="boxcomun">
        		<table id="comun_table"></table>
        		<div id="comun_pager1"></div>
        	</div>
        	
        	<div id="addcomunpay-form" title="Внести оплату">
            	<form>
                	<fieldset>
                		<div id="apart_payment_form">
                            <table class="apart_form_table">
                            	<tr>
                            		<td align="left">
                            			<label><?php print osbb_print_label(27) ?></label>
                            			<div class="helperstrike">(<?php print osbb_print_label(95) ?>)</div> 
                            		</td>
                            		<td><input id="apart_n" type="text"></input></td>
                            	</tr>
                            	<tr>
                            		<td colspan="2" height="5px;">&nbsp;</td>
                            	</tr>
                            	<tr>
                            		<td colspan="2">
                            			<label><?php print osbb_print_label(53) ?></label>
                            			<div class="helperstrike">(<?php print osbb_print_label(96) ?>)</div> 
                            		</td>
                            	</tr>
                            	<tr>
                            		<td align="left"><input type="radio" id="pay_ismonth" name="pays" checked="checked" ><?php print osbb_print_label(54) ?></td>
                            		<td align="left"><input type="radio" id="pay_issumm" name="pays" /><?php print osbb_print_label(20) ?></td>
                            	</tr>
                            	<tr>
                            		<td colspan="2"><input id="pay_amaunt" type="text"></input></td>
                            	</tr>
                            	<tr>
                            		<td colspan="2" height="5px;">&nbsp;</td>
                            	</tr>
                            	<tr>
                            		<td><label><?php print osbb_print_label(55) ?></label></td>
                            		<td><input type="text" id="PayOnDate" /></td>
                            	</tr>
                            	<tr>
                            		<td align="left"><label><?php print osbb_print_label(56) ?></label></td>
                            		<td>
                            			<select id="commers_type">
                            		        <?php print osbb_get_group_combobox(); ?>
                            		    </select>
                            		</td>
                            	</tr>
                            </table>
                        </div>
                	</fieldset>
            	</form>
            	<p style='color: red;' class="validateTips"><?php print osbb_print_label(52) ?></p>
            </div>
            
        	<table class='btncontainer' cellpadding="0" cellspacing="0" width="100%" border="0" style="padding-top: 3px;">
        		<tr>
        			<td align="right" width='680px;'>
        				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="BuildComunGrid();">
        					<span class="ui-button-text">
        	                    <?php print osbb_print_label(60) ?>
        	    			</span>
        				</button>
        			</td>
        			<td>&nbsp;</td>
        		</tr>
        		<tr>
        			<td>
        				<table cellpadding="0" cellspacing="0" width="100%" border="0">
        					<tr>
        						<td style="width: 170px">&nbsp;</td>
        						<td width="220px;">
        							<?php print osbb_print_label(93) ?>:
        							<select id="fltypecomun">
        								<option value="0" selected="selected"><?php print osbb_print_label(58) ?></option>
        								<option value="1">Excel</option>
        							</select>
        						</td>
                    			<td width="180px;">
                    				<?php print osbb_print_label(59) ?>
                    				<select id="typeenccomun">
                    					<option value="0" selected="selected">CP-1251</option>
                    					<option value="1">UTF-8</option>
                    				</select>
                    			</td>
                    			<td align="right" width="155px;">
                    				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  onclick="GetFileComun();">
                    					<span class="ui-button-text">
                    					    <?php print osbb_print_label(57) ?>
                    					</span>
                    				</button>
                    			</td>
                    			<td>&nbsp;</td>
        					</tr>
        				</table>
        			</td>	
        		</tr>
        	</table>
        </fieldset>
	</div>
	<div id="atabs-2">
	<?php $cer = variable_get('osbb_reports_view_roles','2');
		            if(osbb_check_user_role($cer)): ?>
		<div id="debcred_container">
        	<table class='btncontainer' cellpadding="0" cellspacing="0" border="0"  width="100%" style='margin-bottom:3px;'>
        		<tr>
        			<td>
            			<table cellpadding="0" cellspacing="0" width="100%" border="0">
            				<tr>
            					<td align="left" width='114px;'><?php print osbb_print_label(61) ?></td>
                    			<td align="left" width='150px;'>
                    				<input type="text" id="datepickerFrom" />
                    			</td>
                    			<td style='padding-left: 5px;' width="70px;"><?php print osbb_print_label(50) ?></td>
                    			<td align="left" width='150px;'>
                    				<input type="text" id="datepickerTo" />
                    			</td>
                    			<td align="right" width='150px;'>
                    				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="GetDebCredReport();" >
                    				    <span class="ui-button-text">
                    				        <?php print osbb_print_label(60) ?>
                    				    </span>
                    				</button>
                    			</td>
                    			<td>&nbsp;</td>
            				</tr>
            			</table>
        			</td>
        		</tr>
        		<tr>
        			<td>
        				<div id="gtfile_box">
        					<table cellpadding="0" cellspacing="0" width="100%" border="0">
        						<tr>
        							<td align="left" width='114px;'><?php print osbb_print_label(93) ?>:</td>
        							<td align="left" width='150px;'>
        								<select id="fltype" style='width: 135px;'>
        									<option value="0" selected="selected"><?php print osbb_print_label(58) ?></option>
        									<option value="1">Excel</option>
        								</select>
        							</td>
        							<td style='padding-left: 5px;' width="100px;"><?php print osbb_print_label(59) ?></td>
        							<td align="left" width='150px;'>
        								<select id="typeenc" style='width: 135px;'>
        									<option value="0" selected="selected">CP-1251</option>
        									<option value="1">UTF-8</option>
        								</select>
        							</td>
        							<td class="getrptlbl" align="right" width='150px;'>
        								<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="GetRepFile();">
        									<span class="ui-button-text">
        								        <?php print osbb_print_label(57) ?>
        								    </span>
        								</button>
        							</td>
        							<td>&nbsp;</td>
        						</tr>		
        					</table>
        				</div>
        			</td>
        		</tr>
        	</table>
        	<div id="boxgrid">
        		<table id="credit_table"></table>
        		<div id="credit_pager1"></div>
        	</div>
        </div>
	<?php endif;?>
	</div>
	<div id="atabs-3">
	<?php $cer = variable_get('osbb_moderator','2');
		            if(osbb_check_user_role($cer)): ?>
		<table class='btncontainer' cellpadding="0" cellspacing="0" width="100%" border="0" style='margin-bottom:3px;'>
			<tr>
    			<td width='100px'><?php print osbb_print_label(61) ?></td>
    			<td width='160px'>
    				<input type="text" id="dtpDonateFrom" />
    			</td>
    			<td style='padding-left: 5px;' width='40px'><?php print osbb_print_label(50) ?></td>
    			<td width='160px'>
    				<input type="text" id="dtpDonateTo" />
    			</td>
    			<td align="right" width='175px'>
    				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="GetDonateReport();" >
    					<span class="ui-button-text">
    				        <?php print osbb_print_label(60) ?>
    				    </span>
    				</button>
    			</td>
    			<td>&nbsp;</td>
    		</tr>
    		<tr>
    			<td colspan='5' align="right" width='130px'>
    				<button type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="ShowComunPayForm();">
    			        <span class="ui-button-text">
    			            <?php print osbb_print_label(62) ?>
    			        </span>
    				</button>
    			</td>
    			<td>&nbsp;</td>
    		</tr>
    		<tr>
    			<td colspan='5'>
    				<div id="boxgrid2">
						<table id="donation_table"></table>
						<div id="donation_pager"></div>
					</div>
    			</td>
    		</tr>
		</table>
		<?php endif;?>
	</div>
</div>
<!--<button onclick="ShowConfig()">get xml config</button>
--><script type="text/javascript">
<?php
    $aStartDate = osbb_start_date(2);
    $sScriptMonth = $aStartDate['month']-1;
    $sDate=$aStartDate['year'].','.$sScriptMonth.','.$aStartDate['day'];
    $sShortDate=$aStartDate['year'].'-'.$aStartDate['month'];
    print ("StartDate = new Date($sDate);"); 
    print ("from_dt = '$sShortDate';");
?>
	InitDatePicker('datepickerFrom');
	InitDatePicker('datepickerTo');
	InitDatePicker('dtpDonateFrom');
	InitDatePicker('dtpDonateTo');
<?php if(osbb_locale_user()=='ua_ua'): ?>
		$.datepicker.setDefaults($.extend($.datepicker.regional[<?php echo '"uk"' ?>]));
	<?php else: ?>
		$.datepicker.setDefaults($.extend($.datepicker.regional[<?php echo '"ru"' ?>]))
<?php
    endif;
    $aStartDate = osbb_start_date(2);
    $sScriptMonth = $aStartDate['month']-1;
    $sDate=$aStartDate['year'].','.$sScriptMonth.','.$aStartDate['day'];
    print ('$("#dtpDonateFrom").datepicker( "setDate",new Date('.$sDate.') );');
    
    $sStartDate = osbb_get_value('credit_start_view_period');
    $aTmpDate = explode('-',$sStartDate);
    $sTmpMonth = $aTmpDate[1]-1;
    $sDate = $aTmpDate[0].','.$sTmpMonth.','.$aTmpDate[2];
    print ('$("#datepickerFrom").datepicker( "setDate",new Date('.$sDate.') );');
    
?>
</script>
<?php
