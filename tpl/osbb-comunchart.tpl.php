<script type="text/javascript">
<?php
    $sPath2Image = base_path().drupal_get_path('module','osbb').'/css/images/';
    $PayLineImage = $sPath2Image.'pd.png';
    print("var PayLineImage = '$PayLineImage'"); 
?>
</script>
<div id='cdialogbox'>
    <div id='ccmb' title=<?php echo("'".osbb_print_label(92)."'")?> >
    	<table cellpadding="0" cellspacing="0" border="0" width="100%">
    		<tr>
    			<td><label for="ccmb_header"><?php print osbb_print_label(26) ?></label></td>
    			<td><span id='ccmb_header'></span></td>
    		</tr>
    		<tr>
    			<td><label for="ccmb_debt"><?php print osbb_print_label(39) ?></label></td>
    			<td><span id='ccmb_debt'></span></td>
    		</tr>
    		<tr>
    			<td><label for="ccmb_kredit"><?php print osbb_print_label(40) ?></label></td>
    			<td><span id='ccmb_kredit'></span></td>
    		</tr>
    		<tr>
    			<td><label for="ccmb_balance"><?php print osbb_print_label(41) ?></label></td>
    			<td><span id='ccmb_balance'></span></td>
    		</tr>
    		<tr>
    			<td><label for="ccmb_pay"><?php print osbb_print_label(42) ?></label></td>
    			<td><span id='ccmb_pay'></span></td>
    		</tr>
    		<tr>
    			<td><label for="ccmb_payday"><?php print osbb_print_label(105) ?></label></td>
    			<td><span id='ccmb_payday'></span></td>
    		</tr>
    	</table>
    </div>
</div>
<div id="pay_legend"><?php print osbb_print_label(43) ?>
	<table id="pay_legend_tbl">
		<tr>
			<td class="pay_legend_caption"><?php print osbb_print_label(39) ?></td>
			<td class="yespay">&nbsp;</td>
		</tr>
		<tr>
			<td class="pay_legend_caption"><?php print osbb_print_label(40) ?></td>
			<td class="notpay">&nbsp;</td>
		</tr>
		<tr>
			<td class="pay_legend_caption"><?php print osbb_print_label(44) ?></td>
			<td class="payonday">&nbsp;</td>
		</tr>
	</table>
</div>