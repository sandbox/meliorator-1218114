<?php
function osbb_get_value($sValueName)
{
    $sSql="SELECT sett_value FROM {osbb_settings} where sett_name like '$sValueName'";
    $sValue = db_result(db_query($sSql));
    if(isset($sValue) && !empty($sValue))
    {
        return $sValue;
    }
    return 0;
}
function osbb_set_value($p_sValueName,$p_sValue)
{
    $s="SELECT count(sett_value) FROM {osbb_settings} where sett_name like '$p_sValueName'";
    $iValue = db_result(db_query($s));
    if($iValue == 0)
        $sSql="insert into {osbb_settings} (sett_name,sett_value) values ('$p_sValueName','$p_sValue')";
    else
        $sSql="update {osbb_settings} set sett_value='$p_sValue' where sett_name like '$p_sValueName' ";
    return $sSql;
}
function osbb_print_label($iCaptionIndex=9999)
{
    $sLocale = osbb_locale_user();
    $filepath = drupal_get_path('module', 'osbb') .'/i18/'.$sLocale;
    $sContent = file_get_contents($filepath);
    $aContent = explode(';',$sContent);
    if($iCaptionIndex==9999 || count($aContent)<$iCaptionIndex)
        return 'Unknow caption';

    $sResult = '';
    $sResult= $aContent[$iCaptionIndex];
    $sResult=trim($sResult);
    if(empty($sResult))
        $sResult = 'Unknow caption';
        
    return $sResult;
}
function osbb_check_user_role($sRID, &$sUserName='')
{
    global $user;
    $bResult = false;
    $iRID = (int) $sRID;
    $iTmp = 1;
    if(user_is_logged_in())
    {
        $sUserName = $user->name;
        $aRoles = $user->roles;
        if(array_key_exists($iRID,$aRoles))
            $iTmp=$iRID;
    }
    if ($iTmp>=$iRID) {
    	$bResult=true;
    }
    
    return $bResult;
}
function osbb_forbidden_message()
{
    return '<div><h4>'.osbb_print_label(91).'</h4></div>';
}
/**
 * @return string
 */
function osbb_locale_user()
{
    $iCountry = variable_get('osbb_country',0);
    $iLang = variable_get('osbb_lang',0);
    $sResult='ru_ua';//язык_страна
    if($iCountry==1 && $iLang==1)
        $sResult='ua_ua';
    if($iCountry==0 && $iLang==0)
        $sResult='ru_ru';
    return $sResult;
}

/**
 * Получить дату начала отсчета хранения данных
 * @param тип возвращаемых данных 0-DateTime, 1 - string, 2 -array,3 - string for view credits
 * @return The
 */
function osbb_start_date($iResType=0)//0-DateTime, 1 - string, 2 -array,3 - string for view credits
{
    $oRes ='';
    $StartDate = variable_get('osbb_startdate','none'); //початок періоду оплати
    if ($StartDate=='none')
    { 
    	 $StartDate=variable_get('DefaultStartDate');
    	 variable_set('osbb_startdate',$StartDate);
    }
    $sCreditViewDate=variable_get('ViewStartCreditDate','none');
    switch ($iResType) {
    	case 0:
            if(is_array($StartDate))
            {
                $aTmp = $StartDate;
                $StartDate = new DateTime();
                $StartDate->setDate($aTmp['year'],$aTmp['month'],$aTmp['day']);
            }
    	break;
    	case 1:
            if(is_array($StartDate))
            {
                $aTmp = $StartDate;
                $StartDate = $aTmp['year'].'-'.$aTmp['month'].'-'.$aTmp['day'];
            }
    	    break;
        case 3:
            if($sCreditViewDate!='none')
                $StartDate=$sCreditViewDate;
    	    break;
    }
    
    $oRes = $StartDate;
    return $oRes;
}
function osbb_build_sql_filter($sSearchField,$sSearchString,$sSearchOper,$sFilter)
{
    $sResult='1=1';
    if($sSearchField=='pay_name')
        $sSearchField='periodicpayid';
    $aDigitsFields = array('sum','periodicpayid','number','square','correction');//перераховані поля в таблицях типу int, numeric
    $aDateFields = array('date_oper');//перераховані поля в таблицях типу date
    if(!is_null($sSearchField) && !is_null($sSearchString) && !is_null($sSearchOper))
    {
        if(!empty($sSearchString))
        {
            if(in_array($sSearchField,$aDigitsFields))//по числовому полю
            {
                $sSqlOper = $sSearchOper=='cn'?' = ':' <> ';
                $sSqlString = $sSearchString;
            }
            elseif (in_array($sSearchField,$aDateFields))//по полю дати
            {
                $sSqlOper = $sSearchOper=='cn'?' = ':' <> ';
                $sSqlString = "'$sSearchString'";
            }
            else//по текстовому полю 
            {
                $sSqlOper = $sSearchOper=='cn'?' like ':' not like ';
                $sSqlString = "'%$sSearchString%'";
            }
            $sResult=" $sSearchField $sSqlOper $sSqlString";
        }
    }
    return $sResult;
}
function osbb_table_width()
{
     $iWidth = variable_get('osbb_table_width',800);
     return $iWidth;
}
function osbb_exec_query_to_object($sSql,$iCurPage, $iRowOnPage, $sIDField, $aFieldsName)
{
    $iCounterRows=0;
    $iTotalSum=0;
    $aRes = array();
    $result = db_query($sSql);
     while ($oRow=db_fetch_array($result)) {
         $aRes[$iCounterRows] = $oRow;
         $iCounterRows++;
    }
    
    $aResultRows = array();
    $i=0;
    $start = $iRowOnPage*$iCurPage - $iRowOnPage;
    $end=($start+$iRowOnPage+1>$iCounterRows?$iCounterRows:$start+$iRowOnPage+1);
    for ($index = $start; $index < $end; $index++) {
        $aResultRows[$i]['id']=$aRes[$index][$sIDField];
        
        $aCells = array($aRes[$index][$sIDField]);
        foreach ($aFieldsName as $sFName) {
            $sBody = str_replace("'",'`',$aRes[$index][$sFName]);//Міняємо ' на ` 
        	array_push($aCells,$sBody) ;
        }
        
        $aResultRows[$i]['cell']=$aCells;
        $i++;
    }
    
    $oResponse = new stdClass();
    $oResponse->page=$iCurPage;
    $oResponse->total=round($iCounterRows/$iRowOnPage,0)+1;
    $oResponse->records=$iCounterRows;
    $oResponse->rows = $aResultRows;
    return $oResponse; 
}

/**
 * Разница в месяцах, между двумя датами
 * @param string Начальная дата 
 * @param string Конечная дата 
 * @return number
 */
function osbb_diff_in_month($sDateFrom, $sDateTo)
{
    $StartDate = new DateTime($sDateFrom) ;//початок оплати
    $ToDayDate = new DateTime($sDateTo);
    $interval = $StartDate->diff($ToDayDate);
    $Year = $interval->format('%Y');
    $Month = $interval->format('%m');
    $iCountMonth = $Month + $Year*12+1;//скільки часу пройшло, в місяцях
    return $iCountMonth;
}