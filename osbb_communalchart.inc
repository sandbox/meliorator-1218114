<?php
function osbb_communal_chart()
{
    $StartDate = osbb_start_date(0);
    $ToDayDate = new DateTime();
    $interval = $StartDate->diff($ToDayDate);
    $Year = $interval->format('%Y');
    $Month = $interval->format('%m');
    $iToDayMonthCount = $Month + $Year*12+1;//скільки часу пройшло, в місяцях
    
    $sResult='<div id="comuntabs">';
    $sSql = "select * from {osbb_premise_groups}";
    $oResult = db_query($sSql);
    $iConter=0;
    $sHeaderTag ='<ul>';
    $aBody = array();
    while ($oRow=db_fetch_array($oResult)) {
        $sTagID = "comuntabs-$iConter";
        $sHeaderTag .="<li><a href=#$sTagID>".$oRow['group_name'].'</a></li>';
        $iGroupID = $oRow['premisegroupid'];
        $TmpArray = osbb_communal_chart_array($iGroupID, $iToDayMonthCount);
        $sDateUpdate = (!isset($oRow['update'])||empty($oRow['update'])?'01.01.1900':date('Y-m-d',strtotime($oRow['update'])));
        $sTableCaption = osbb_print_label(12).'.'.$oRow['group_name'].' ('.osbb_print_label(13).' '.$sDateUpdate.')';
        $aBody[$sTagID]="<div id='$sTagID'>".osbb_communal_chart_html($iToDayMonthCount,$TmpArray,$iGroupID,$sTableCaption).'</div>';
        $iConter++;
    }
    $sHeaderTag .='</ul>';
	$sResult .=$sHeaderTag;
	foreach ($aBody as $sContent) {
		$sResult .= $sContent;
	}
	$sResult .='</div>';
	$sResult .= theme('osbb_comunchart');
    return $sResult;
}

function osbb_communal_chart_array($p_sPayGroupID, $p_iToDayMonthCount)
{
    $aPayArray = array();
    $dSinglePay = osbb_get_value('osbbpayon');
    $sSql = "SELECT p.premiseid, `number` as `apartment`, `square`,`ownerid`, ifnull(paid_month,0) as `month`,
    ifnull(balance,0) as `balance`, `square`*(ifnull(paid_month,0)) as `debt`,
    if(`square`*($p_iToDayMonthCount-(ifnull(paid_month,0)))<0,0,`square`*($p_iToDayMonthCount-(ifnull(paid_month,0))))  as `kredit` 
	FROM {osbb_premise} p left outer join {osbb_communal_helper} ch on p.premiseid=ch.premiseid 
	WHERE premisegroupid=$p_sPayGroupID order by `number`";
    	 
     $oResult = db_query($sSql);
     while ($oPay=db_fetch_object($oResult)) {
         $iPayMonth = $oPay->month;
         $iNotPayMonth = $p_iToDayMonthCount - $iPayMonth;
         if($iPayMonth>0)
             $aPayMonth = array_fill(1, $iPayMonth, 1);
         else
            $aPayMonth = array();
         if($iNotPayMonth>0)
            $aNotPayMonth = array_fill($iPayMonth + 1, $iNotPayMonth, 0);
         else
            $aNotPayMonth = array();
         $aPayArray[$oPay->apartment]=$aPayMonth + $aNotPayMonth;
         $aPayArray[$oPay->apartment]['pay']=$oPay->square*$dSinglePay;
         $aPayArray[$oPay->apartment]['balance'] = $oPay->balance;
         $aPayArray[$oPay->apartment]['debt'] = $oPay->debt*$dSinglePay;
         $aPayArray[$oPay->apartment]['kredit'] = $oPay->kredit*$dSinglePay;
    }
    return $aPayArray;
}
function osbb_communal_chart_html($iToDayMonth,$aExten,$iGroupID,$sCaption='caption')
{
    $RussianMonth = array('January'=>'Январь','February'=>'Февраль','March'=>'Март','April'=>'Апрель','May'=>'Май','June'=>'Июнь',
					'July'=>'Июль','August'=>'Август','September'=>'Сентябрь','October'=>'Октябрь','November'=>'Ноябрь','December'=>'Декабрь');
    $UkrainMonth = array('January'=>'Січень','February'=>'Лютий','March'=>'Березень','April'=>'Квітень','May'=>'Травень','June'=>'Червень',
					'July'=>'Липень','August'=>'Серпень','September'=>'Вересень','October'=>'Жовтень','November'=>'Листопад','December'=>'Грудень');
    if(osbb_locale_user()=='ua_ua')
        $MonthTranslate = $UkrainMonth;
    else
        $MonthTranslate = $RussianMonth;
    $ToDayDate = new DateTime();
    
    $sResult = '<table class="payment_table">';
    $sResult .="<caption>$sCaption</caption>";
    $sResult .= '<tr><th>Дата/Квартира</th>';
    
    //Генерація номерів квартир
    foreach ($aExten as $iAppart=>$aApart) {
    	$sPay = $aExten[$iAppart]['pay'];
        $sBalance = $aExten[$iAppart]['balance'];
        $sDebt=$aExten[$iAppart]['debt'];
        $sKredit = $aExten[$iAppart]['kredit'];
        $sLinkName = 'apart'.$iGroupID.'_'.$iAppart;
        $sResult .= "<th class='ahead' onclick='PayTableHeader(this)' linkname='$sLinkName' name='$iAppart' debt='$sDebt' kredit='$sKredit' pay='$sPay' balance='$sBalance' >$iAppart</th>";
    }
    $sResult .= '<th class=\'ahead\'>#</th>';
    
    $sResult .= '</tr>';
    $i=$iToDayMonth;
    $iMonthCurrent = $ToDayDate->format('m')+0;//поточний місяць для статистики
    $iYearCurrent = $ToDayDate->format('Y')+0;//поточний рік для статистики
    $iMonthCounter = $ToDayDate->format('m')+0; //поточний місяць, потрібен для лічильника
    $iYearCounter = $ToDayDate->format('Y')+0;//поточний рік, для лічільника
    for ($iMonth = $iToDayMonth; $iMonth > 0; $iMonth --) {
        $TmpM = date('F', mktime(0, 0, 0, $iMonthCounter,1));
        if (array_key_exists($TmpM, $MonthTranslate))
            $TmpM = $MonthTranslate[$TmpM];
        $sResult .= "<tr><td class=\"month_name\">$iYearCounter $TmpM</td>";
        foreach ($aExten as $iAppart=>$aApart) {
            $scl =  $aExten[$iAppart][$iMonth]==0?'notpay':'yespay';
            $iID=$iYearCounter.'_'.$iMonthCounter.'_'.$iGroupID.'_'.$iAppart;
            $sName = 'apart'.$iGroupID.'_'.$iAppart;
            $sResult .= "<td id='$iID' class=\"$scl apart_column\" name='$sName'>&nbsp;</td>";
        }
        $sResult .= "<td>$i</td>";
        $i--;
        $iMonthCounter --;
        if ($iMonthCounter < 1) {
            $iYearCounter --;
            $iMonthCounter = 12;
        }
        $sResult .= '</tr>';
    }
    
    $sResult .= '</table>';
    
    return $sResult;
}