<?php
function osbb_response_json ($iCommand)
{
    drupal_set_header('Content-Type: text/x-json; charset=UTF-8');
    osbb_include_libs();
    try {
        $is_moder = false;
        //Перевірка на роль модератора
        $cer = variable_get('osbb_moderator', '2');
        $sUserName = '';
        if (osbb_check_user_role($cer, $sUserName))
            $is_moder = true;
            //Встановлення sql фільтра
        $sSqlFilter = '1=1';
        if (isset($_REQUEST['_search']))
            $sSqlFilter = osbb_build_sql_filter($_REQUEST['searchField'], $_REQUEST['searchString'], $_REQUEST['searchOper'], $_REQUEST['filters']);
        $oRespose = new stdClass();
        switch ($iCommand) {
            case 1: //12 osbb_accounting
                $oResult = osbb_accounting_payments($_REQUEST['dtFrom'], $_REQUEST['dtTo'], $_REQUEST['sidx'], $_REQUEST['sord'], $_REQUEST['page'], $_REQUEST['rows'], $sSqlFilter);
                break;
            case 2: //10 osbb_accounting
                $oResult = osbb_accounting_communal($_REQUEST['dtFrom'], $_REQUEST['dtTo'], $_REQUEST['sidx'], $_REQUEST['sord'], $_REQUEST['page'], $_REQUEST['rows'], $is_moder);
                break;
            case 3: //13 osbb_accounting
                if ($is_moder) {
                    osbb_save_credit($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['date_oper'], $_REQUEST['sum'], $_REQUEST['reason'], $_REQUEST['note'], $_REQUEST['pay_name'], $sUserName);
                }
                break;
            case 4: //11 osbb_accounting
                $oResult = osbb_save_communal_pay($_REQUEST['prem'], $_REQUEST['summa'], $_REQUEST['groupid'], $_REQUEST['ismonth'], $_REQUEST['ondate'], $sUserName);
                break;
            case 5: //14 osbb_accounting
                $oResult = osbb_accounting_donate($_REQUEST['dtFrom'], $_REQUEST['dtTo'], $_REQUEST['sidx'], $_REQUEST['sord'], $_REQUEST['page'], $_REQUEST['rows'], $sSqlFilter);
                break;
            case 6: //15 osbb_accounting
                if ($is_moder) {
                    osbb_save_donate($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['date_oper'], $_REQUEST['sum'], $_REQUEST['reason'], $_REQUEST['note'], $_REQUEST['pay_name'], $sUserName);
                }
                break;
            case 7: //72
                $oResult = osbb_communal_payday();
                break;
            case 8: //71
                $oResult = osbb_save_settings($_REQUEST['payon'], $_REQUEST['groupid'], $_REQUEST['startview']);
                break;
            case 9: //11
                $oResult = osbb_group_table($_REQUEST['page'], $_REQUEST['rows'],$sSqlFilter);
                break;
            case 10: //12
                osbb_save_group($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['group_name'], $_REQUEST['premisetypeid']);
                break;
            case 11: //41
                $oResult = osbb_premise_table($_REQUEST['page'], $_REQUEST['rows'],$sSqlFilter);
                break;
            case 12: //42
                osbb_save_premise($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['fio'], $_REQUEST['square'], $_REQUEST['number'], $_REQUEST['group_name'], $_REQUEST['correction']);
                break;
            case 13: //21
                $oResult = osbb_types_table($_REQUEST['page'], $_REQUEST['rows'],$sSqlFilter);
                break;
            case 14: //22
                osbb_save_type($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['type_name']);
                break;
            case 15: //31
                $oResult = osbb_owners_table($_REQUEST['page'], $_REQUEST['rows'],$sSqlFilter);
                break;
            case 16: //32
                osbb_save_owner($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['first_name'], $_REQUEST['last_name'], $_REQUEST['middle_name'], $_REQUEST['email'], $_REQUEST['phone_local'], $_REQUEST['phone_mobile']);
                break;
            case 17: //51
                $oResult = osbb_periodic_table($_REQUEST['page'], $_REQUEST['rows'],$sSqlFilter);
                break;
            case 18: //52
                osbb_save_periodic($_REQUEST['oper'], $_REQUEST['id'], $_REQUEST['pay_name']);
                break;
            case 19:
                break;
            default:
                $oRespose->ErrorMessage = 'Unrecognized code command';
        }
    } catch (Exception $e) {
        $oRespose->ErrorMessage = $e->getMessage();
    }
    $sResponse = drupal_json($oResult);
    
    isset($sResponse) ? print $sResponse : print '';
    exit();
}
//читаємо налаштування із xml для таблиць
function osbb_response_xml ($iCommand)
{
    osbb_include_libs();
    $sResponse = '';
    switch ($iCommand) {
        case 1:
            $sResponse = osbb_settings_group();
            break;
        case 2:
            $sResponse = osbb_settings_premise();
            break;
        case 3:
            $sResponse = osbb_settings_accounting_credit($_REQUEST['dtFrom'], $_REQUEST['dtTo']);
            break;
        case 4:
            $sResponse = osbb_settings_payments($_REQUEST['dtFrom'], $_REQUEST['dtTo']);
            break;
        case 5:
            $sResponse = osbb_settings_commun($_REQUEST['dtFrom'], $_REQUEST['dtTo']);
            break;
        case 6:
            $sResponse = osbb_settings_types();
            break;
        case 7:
            $sResponse = osbb_settings_owners();
            break;
        case 8:
            $sResponse = osbb_settings_periodic();
            break;
    }
    drupal_set_header('Content-Type: text/xml; charset=UTF-8');
    print $sResponse;
    exit();
}
function osbb_response_file ($iCommand)
{
    osbb_include_libs();

    $cer = variable_get('osbb_moderator', '2');
    $sUserName = '';
    if (osbb_check_user_role($cer, $sUserName))
        $is_moder = true;
            
    $sResponse = '';
    switch ($iCommand) {
        case 1:
            $flname = 'comunalka_report.txt';
            if ($_REQUEST['tp'] == '1')
                $flname = 'comunalka_report.csv';
            $sResponse = osbb_communalka_file($_REQUEST['tp'], $_REQUEST['enc'], $is_moder);
            break;
        case 2:
            $flname = 'osbb_report.txt';
            if ($_REQUEST['tp'] == '1')
                $flname = 'osbb_report.csv';
            $sResponse = osbb_debcred_file($_REQUEST['tp'], $_REQUEST['enc']);
            drupal_set_header('Content-type: application/x-download');
            drupal_set_header('Content-Length: ' . strlen($Response));
            drupal_set_header('Content-Disposition: attachment; filename="' . $flname . '"');
            break;
    }
    drupal_set_header('Content-type: application/x-download');
    drupal_set_header('Content-Length: ' . strlen($sResponse));
    drupal_set_header('Content-Disposition: attachment; filename="' . $flname . '"');
    print $sResponse;
    exit();
}
function osbb_include_libs()
{
    require_once (drupal_get_path('module', 'osbb').'/osbb_accounting.inc');
    require_once (drupal_get_path('module', 'osbb').'/osbb_parameters.inc');
    require_once (drupal_get_path('module', 'osbb').'/osbb_getfiles.inc');
    require_once (drupal_get_path('module', 'osbb').'/osbb_common.inc');
    require_once (drupal_get_path('module', 'osbb').'/osbb_table_settings.inc');
}