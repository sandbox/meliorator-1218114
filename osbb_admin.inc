<?php
function osbb_admin_settings ()
{
    require_once (drupal_get_path('module', 'osbb').'/osbb_common.inc');
    $country = variable_get('osbb_country',0);
    $lang = variable_get('osbb_lang',0);
    $sdt = variable_get('osbb_startdate','none');
    $cvr = variable_get('osbb_anonymous','1');
    $cer = variable_get('osbb_moderator','2');
    $rvr = variable_get('osbb_reports_view_roles','2');
    $cut = variable_get('osbb_comunal_ui_themes','1');
    $iWidth = variable_get('osbb_table_width',800);
    
    if ($sdt=='none') {
    	 $sdt=variable_get('DefaultStartDate','2008-10-01');
    	 $atimestamp =getdate(strtotime($sdt));
    	 $sdt=array('day'=>$atimestamp['mday'],'month'=>$atimestamp['mon'],'year'=>$atimestamp['year']);
    }
    $aroles = user_roles();
    $aCountries = array('Россия','Україна');
    $aLanguages = array('русский','українська');
    $athemes=array();
    $sBaseCSSPath=drupal_get_path('module','osbb').'/css/';
    $afiles = scandir($sBaseCSSPath);
    foreach ($afiles as $path) {
    	if(is_dir($sBaseCSSPath.$path))
    	{
    	    $ainnerfiles = glob($sBaseCSSPath.$path.'/jquery*.css');
    	    if (count($ainnerfiles)>0) {
    	    	$athemes[]=$path;
    	    }
    	}
    }
    
    $form=array();
    $form['osbb_country']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(2),
    	'#options'=>$aCountries,
        '#default_value'=>$country
    );
    $form['osbb_lang']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(3),
    	'#options'=>$aLanguages,
        '#default_value'=>$lang
    );
    $form['osbb_startdate']=array(
        '#type'=>'date',
    	'#title'=>osbb_print_label(4),
        '#default_value'=>$sdt
    );
    $form['osbb_anonymous']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(5),
        '#options'=>$aroles,
    	'#default_value'=>$cvr
    );
    $form['osbb_reports_view_roles']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(6),
        '#options'=>$aroles,
    	'#default_value'=>$rvr
    );
    $form['osbb_moderator']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(7),
        '#options'=>$aroles,
    	'#default_value'=>$cer
    );
    $form['osbb_comunal_ui_themes']=array(
        '#type'=>'select',
    	'#title'=>osbb_print_label(8),
        '#options'=>$athemes,
    	'#default_value'=>$cut
    );
    $form['osbb_table_width']=array(
        '#type'=>'textfield',
    	'#title'=>osbb_print_label(97),
        '#after_field'=>'px',
    	'#size'=>'4',
    	'#default_value'=>$iWidth
    );
    
    return system_settings_form($form);
}