<?php
function osbb_settings_periodic()
{
    $path = drupal_get_path('module', 'osbb') .'/settings/settings_periodic_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    $oDoc->grid->url = base_path().$oDoc->grid->url;
    $oDoc->grid->editurl = base_path().$oDoc->grid->editurl;
    //Переклад назв стовбців    
    $oDoc->grid->colNames[1]=osbb_print_label(14);
    
    $oDoc->grid->caption=osbb_print_label(34);
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_types()
{
    $path = drupal_get_path('module', 'osbb') .'/settings/settings_types_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    $oDoc->grid->url = base_path().$oDoc->grid->url;
    $oDoc->grid->editurl = base_path().$oDoc->grid->editurl;
    //Переклад назв стовбців    
    $oDoc->grid->colNames[1]=osbb_print_label(14);
    
    $oDoc->grid->caption=osbb_print_label(32);
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_owners()
{
    $path = drupal_get_path('module', 'osbb') .'/settings/settings_owners_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    $oDoc->grid->url = base_path().$oDoc->grid->url;
    $oDoc->grid->editurl = base_path().$oDoc->grid->editurl;
    //Переклад назв стовбців    
    $oDoc->grid->colNames[1]=osbb_print_label(86);
    $oDoc->grid->colNames[2]=osbb_print_label(87);
    $oDoc->grid->colNames[3]=osbb_print_label(88);
    $oDoc->grid->colNames[5]=osbb_print_label(89);
    $oDoc->grid->colNames[6]=osbb_print_label(90);
    
    $oDoc->grid->caption=osbb_print_label(33);
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_commun($sFrom, $sTo)
{
    $path = drupal_get_path('module', 'osbb') .'/settings/commun_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    
    $sUrl = base_path().'osbb_request_json/2?dtFrom='.$sFrom.'&'.'dtTo='.$sTo;
    $oDoc->grid->url =$sUrl; 
    
    //Переклад назв стовбців    
    $oDoc->grid->colNames[2]=osbb_print_label(27);//Owner
    $oDoc->grid->colNames[3]=osbb_print_label(15);//Type
    $oDoc->grid->colNames[4]=osbb_print_label(80);
    $oDoc->grid->colNames[5]=osbb_print_label(81);
    $oDoc->grid->colNames[6]=osbb_print_label(70);
    $oDoc->grid->colNames[7]=osbb_print_label(71);
    $oDoc->grid->colNames[8]=osbb_print_label(82);
    $oDoc->grid->colNames[9]=osbb_print_label(83);
    
    $oDoc->grid->caption=osbb_print_label(84);//ComunTable
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_payments($sFrom, $sTo)
{
    $path = drupal_get_path('module', 'osbb') .'/settings/payments_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    
    $sUrl = base_path().'osbb_request_json/5?dtFrom='.$sFrom.'&'.'dtTo='.$sTo;
    $sEditUrl = base_path().'osbb_request_json/6?dtFrom='.$sFrom.'&'.'dtTo='.$sTo;
    $oDoc->grid->url=$sUrl;
    $oDoc->grid->editurl=$sEditUrl;
    
    //Переклад назв стовбців
    $oDoc->grid->colNames[1]=osbb_print_label(78);//PIB
    $oDoc->grid->colNames[2]=osbb_print_label(20);//Summ
    $oDoc->grid->colNames[3]=osbb_print_label(21);//Date
    
    $oDoc->grid->caption=osbb_print_label(77);//PayTableB
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_accounting_credit($sFrom, $sTo)
{
    $path = drupal_get_path('module', 'osbb') .'/settings/accounting_credit.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    
    $sUrl = base_path().'osbb_request_json/1?dtFrom='.$sFrom.'&'.'dtTo='.$sTo;
    $sEditUrl = base_path().'osbb_request_json/3?dtFrom='.$sFrom.'&'.'dtTo='.$sTo;
    $oDoc->grid->url=$sUrl;
    $oDoc->grid->editurl=$sEditUrl;
    
    $aValues = $oDoc->xpath("//colModel[name='pay_name']/editoptions/value");
    if($aValues && count($aValues)>0)
    {
        $oVal = $aValues[0];
        $oVal->addChild('p0','');
        $sSql="SELECT * FROM {`osbb_periodic_pays`} where periodicpayid not in (select sett_value from {osbb_settings} where sett_name='comunal_periodic_id')";
        $result = db_query($sSql);
        while ($oRow=db_fetch_array($result)) {
            $oVal->addChild('p'.$oRow['periodicpayid'],$oRow['pay_name']);
        }
    }
    
    //Переклад назв стовбців
    $oDoc->grid->colNames[1]=osbb_print_label(20);//Summ
    $oDoc->grid->colNames[2]=osbb_print_label(21);//Date
    $oDoc->grid->colNames[3]=osbb_print_label(22);//Reason
    $oDoc->grid->colNames[4]=osbb_print_label(23);//Note
    $oDoc->grid->colNames[5]=osbb_print_label(24);//Periodic
    
    $oDoc->grid->caption=osbb_print_label(25);//PayTable
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_group()
{
    $path = drupal_get_path('module', 'osbb') .'/settings/settings_group_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    $oDoc->grid->url = base_path().$oDoc->grid->url;
    $oDoc->grid->editurl = base_path().$oDoc->grid->editurl;
    $aValues = $oDoc->xpath("//colModel[name='premisetypeid']/editoptions/value");
    if($aValues && count($aValues)>0)
    {
        $oVal = $aValues[0];
        $oVal->addChild('p0','');
        $sSql='SELECT * FROM {osbb_premise_types}';
        $result = db_query($sSql);
        while ($oRow=db_fetch_array($result)) {
            $oVal->addChild('p'.$oRow['premisetypeid'],$oRow['type_name']);
        }
    }
    
    //Переклад назв стовбців
    $oDoc->grid->colNames[1]=osbb_print_label(14);//Name
    $oDoc->grid->colNames[2]=osbb_print_label(15);//Type
    $oDoc->grid->caption=osbb_print_label(16);//GroupA
    
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
  
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}
function osbb_settings_premise()
{
    $path = drupal_get_path('module', 'osbb') .'/settings/settings_premise_table.xml';
    $oDoc = new SimpleXMLElement($path, NULL, TRUE);
    $oDoc->grid->url = base_path().$oDoc->grid->url;
    $oDoc->grid->editurl = base_path().$oDoc->grid->editurl;
    $aValues = $oDoc->xpath("//colModel[name='fio']/editoptions/value");
    if($aValues && count($aValues)>0)
    {
        $oVal = $aValues[0];
        $sSql="SELECT *,concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.') as fio 
        		FROM {osbb_owners} order by `first_name`,`last_name`";
        $result = db_query($sSql);
        while ($oRow=db_fetch_array($result)) {
           $oVal->addChild('p'.$oRow['ownerid'],$oRow['fio']);
        }
    }
    
    $aValues = $oDoc->xpath("//colModel[name='group_name']/editoptions/value");
    if($aValues && count($aValues)>0)
    {
        $oVal = $aValues[0];
        $sSql="SELECT * FROM {osbb_premise_groups}";
        $result = db_query($sSql);
        while ($oRow=db_fetch_array($result)) {
           $oVal->addChild('p'.$oRow['premisegroupid'],$oRow['group_name']);
        }
    }
    
    //Переклад назв стовбців
    $oDoc->grid->colNames[1]=osbb_print_label(26);//NumApart
    $oDoc->grid->colNames[2]=osbb_print_label(27);//Owner
    $oDoc->grid->colNames[3]=osbb_print_label(28);//Square
    $oDoc->grid->colNames[4]=osbb_print_label(29);//Group
    $oDoc->grid->colNames[5]=osbb_print_label(30);//Correct
    
    $oDoc->grid->caption=osbb_print_label(31);//Apart
    $oDoc->grid->recordtext=osbb_print_label(17);//RecordText
    $oDoc->grid->emptyrecords=osbb_print_label(18);//EmptyRecord
    $oDoc->grid->loadtext=osbb_print_label(19);//LoadText
    
    $oDoc->grid->width=osbb_table_width();
    
    return  $oDoc->asXML();
}