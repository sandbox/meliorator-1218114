function osbb_request_url(sRequestType, sCommand, sAdditionalParams)
{
	if(typeof(sAdditionalParams)!="undefined" && sAdditionalParams !="")
	{
		sAdditionalParams = '?'+sAdditionalParams;
	}
	else
	{
		sAdditionalParams='';
	}
	sResponse = '';
	switch (sRequestType) {
		case 'json':
			sResponse=Drupal.settings.basePath+'osbb_request_json/'+sCommand+sAdditionalParams;
		break;
		case 'xml':
			sResponse=Drupal.settings.basePath+'osbb_request_xml/'+sCommand+sAdditionalParams;
		break;
		case 'file':
			sResponse=Drupal.settings.basePath+'osbb_request_file/'+sCommand+sAdditionalParams;
			break;
	}
	if(sResponse == '')
		alert('Unknow request type');
	return sResponse;
}
function osbb_date_url_params(sFromID, sToID)
{
	sResult='';
	if(typeof(sFromID)=="undefined" || typeof(sToID)=="undefined")
	{
		return sResult;
	}
	
	dtFrom =$("#"+sFromID).datepicker( "getDate" );
	dtTo =$("#"+sToID).datepicker( "getDate" );
	
	sFrom = dtFrom.getFullYear()+'-'+String(Number(dtFrom.getMonth()+1))+'-'+dtFrom.getDate();
	sTo = dtTo.getFullYear()+'-'+String(Number(dtTo.getMonth()+1))+'-'+dtTo.getDate();
	
	sResult = "dtFrom="+sFrom+"&dtTo="+sTo;
	return sResult; 
}