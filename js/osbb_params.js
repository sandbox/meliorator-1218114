jQuery(document).ready(function(){
	jQuery('#sttabs').tabs();
	InitGroupsGrid();
	InitTypesGrid();
	InitOwnersGrid();
	InitPremiseGrid();
	InitPeriodicGrid();
});
function SaveOSBBSettings()
{
	ssurl=osbb_request_url('json','8');//'osbb_request_json/8';
	var tpayon=$("#payon").val();
	var tcgrp = $("select#comperiodname").val();
	var tmpvdt=$("#dtpStartDate").datepicker( "getDate" );
	tvdt = tmpvdt.getFullYear()+'-'+(tmpvdt.getMonth()+1)+'-'+tmpvdt.getDate();
	$.getJSON(ssurl,{payon:tpayon,groupid:tcgrp,startview:tvdt},function(data){
			if(data.ErrorMessage)
			{ alert(data.ErrorMessage); }
			else
			{ alert(data.Result); }
	});
}
function InitGroupsGrid()
{
	grseturl=osbb_request_url('xml','1');//+'osbb_request_xml/1';
	$("#settings_group_table").jqGrid('jqGridImport',{impurl:grseturl,importComplete:SetGroupPager}); 
}

function InitTypesGrid()
{	
	tgseturl=osbb_request_url('xml','6');//Drupal.settings.basePath+'osbb_request_xml/6';
	$("#settings_types_table").jqGrid('jqGridImport',{impurl:tgseturl,importComplete:SetTypePager});
}
function InitOwnersGrid()
{		
	ogseturl=osbb_request_url('xml','7');//Drupal.settings.basePath+'osbb_request_xml/7';
	$("#settings_owners_table").jqGrid('jqGridImport',{impurl:ogseturl,importComplete:SetOwnersPager}); 
}
function InitPremiseGrid()
{
	prseturl=osbb_request_url('xml','2');//Drupal.settings.basePath+'osbb_request_xml/2';
	$("#settings_premise_table").jqGrid('jqGridImport',{impurl:prseturl,importComplete:SetPremisePager}); 
}
function InitPeriodicGrid()
{
	pegseturl=osbb_request_url('xml','8');//Drupal.settings.basePath+'osbb_request_xml/8';
	$("#settings_periodic_table").jqGrid('jqGridImport',{impurl:pegseturl,importComplete:SetPeriodicPager});
	
}
function SetPeriodicPager()
{
	SetPager('settings_periodic_table','settings_periodic_pager');
}
function SetPremisePager()
{
	SetPager('settings_premise_table','settings_premise_pager');
}
function SetGroupPager()
{
	SetPager('settings_group_table','settings_group_pager');
}
function SetTypePager()
{
	SetPager('settings_types_table','settings_types_pager');
}
function SetOwnersPager()
{
	SetPager('settings_owners_table','settings_owners_pager');
}
function SetPager(table_id,pager_id)
{
	jQuery('#'+table_id).jqGrid('navGrid','#'+pager_id,{edit:true,add:true,del:true},
			{width:390,savekey: [true,13], navkeys: [true,38,40], closeAfterEdit:true,reloadAfterSubmit:true, closeOnEscape:true, bottominfo:Message1},	
			{width:390,savekey: [true,13], navkeys: [true,38,40], closeAfterAdd:true,reloadAfterSubmit:true, closeOnEscape:true, bottominfo:Message1},
			{},{multipleSearch:false,sopt:['cn','nc']});
}
function ShowConfig()
{
	//alert($("#settings_periodic_table").jqGridExport({exptype:"xmlstring" }));
}