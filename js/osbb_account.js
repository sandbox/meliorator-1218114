var from_dt,to_dt, StartDate, CurrentDate;
var isComunLoaded = false;
var isLoaded = false;
var isDonateLoaded = false;
AddComunPay=null;
jQuery(document).ready(function(){
	jQuery('#atabs').tabs();
	CurrentDate = new Date();
	//Month calendar from
	fromCal = $('#fromDate').addCal(
			{selD:StartDate,
			 calType:1,
			 click:selectFromDate
			}
		);
	//Month calendar to
	$('#toDate').addCal(
			{selD:CurrentDate,
			 calType:1,
			 click:selectToDate
			}
		);
	$('#addcomunpay-form').dialog({ 
		autoOpen: false,
		width: 330,
		modal:true,
		buttons: {
			"Внести": AddComunPay,
			Cancel: function() {
				$(this).dialog( "close" );
			}
		}
	});
	$('div:has(#addcomunpay-form)').css('font-size','14px');
	today=new Date();
	to_dt=today.getFullYear()+"-"+Number(today.getMonth()+1);
	
	$("#PayOnDate").datepicker({dateFormat: 'dd.mm.yy'});
	$("#PayOnDate").datepicker( "setDate", new Date() );
	$("#datepickerTo").datepicker( "setDate",CurrentDate );
	$("#dtpDonateTo").datepicker( "setDate",CurrentDate );
	
	$('#gtfile_box').hide();
	BuildComunGrid();
});
function InitDatePicker(sDTPId)
{
	$("#"+sDTPId).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd.mm.yy'
	});
}
function checkLength( o, n, min, max ) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass( "ui-state-error" );
		updateTips( "Length of " + n + " must be between " +
			min + " and " + max + "." );
		return false;
	} else {
		return true;
	}
}
function mysum(val, name, record)
{
	tmp1 = new Number(parseFloat(val||0));
	tmp2 = new Number(parseFloat((record[name]||0)));
	tmp3= new Number(tmp1+tmp2);
	return  tmp3.toFixed(2);
}
function ConvertCalDate(dt)
{
	tmp = new Number(dt.m);
	tmp += 1;
	return dt.y+"-"+tmp;
}
function selectToDate(obj,dt)
{
	to_dt = ConvertCalDate(dt);
}
function selectFromDate(obj,dt)
{
	from_dt = ConvertCalDate(dt);
}
function ShowComunPayForm()
{
	$('#addcomunpay-form').dialog('open');
}
function BuildReportGrid()
{
	/*
	dtFrom =$("#datepickerFrom").datepicker( "getDate" );
	dtTo =$("#datepickerTo").datepicker( "getDate" );
	
	sFrom = dtFrom.getFullYear()+'-'+String(Number(dtFrom.getMonth()+1))+'-'+dtFrom.getDate();
	sTo = dtTo.getFullYear()+'-'+String(Number(dtTo.getMonth()+1))+'-'+dtTo.getDate();*/
	url_params = osbb_date_url_params("datepickerFrom","datepickerTo");
	
	acturl=osbb_request_url('json','1',url_params);//"osbb_request_json/1?dtFrom="+sFrom+"&dtTo="+sTo;
	actediturl = "";
	
	if(isLoaded)
	{
		$("#credit_table").jqGrid('clearGridData');
		
		$("#credit_table").setGridParam({url:acturl}); 
		$("#credit_table").trigger("reloadGrid");
		return;
	}
	
	acseturl=osbb_request_url('xml','3',url_params);//"osbb_request_xml/3?dtFrom="+sFrom+"&dtTo="+sTo;
	$("#credit_table").jqGrid('jqGridImport',{impurl:acseturl,importComplete:SetUpCreditTable});
}
function SetUpCreditTable()
{
	isLoaded = true;
	jQuery("#credit_table").jqGrid('navGrid','#credit_pager1',{edit:false,add:false,del:false},
			{}, {},	{},{multipleSearch:false,sopt:['cn','nc']});
}
function BuildComunGrid()
{
	$("#comun_table").jqGrid('clearGridData');
	
	comunurl=osbb_request_url('json','2',"dtFrom="+from_dt+"&dtTo="+to_dt);//"osbb_request_json/2?dtFrom="+from_dt+"&dtTo="+to_dt;
	if(isComunLoaded)
	{
		$("#comun_table").setGridParam({url:comunurl}); 
		$("#comun_table").trigger("reloadGrid");
		return;
	}
	
	comunseturl=osbb_request_url('xml','5',"dtFrom="+from_dt+"&dtTo="+to_dt);//"osbb_request_xml/5?dtFrom="+from_dt+"&dtTo="+to_dt;
	$("#comun_table").jqGrid('jqGridImport',{impurl:comunseturl,importComplete:SetUpComunTable});
}
function SetUpComunTable()
{
	isComunLoaded = true;
	jQuery("#comun_table").jqGrid('navGrid','#credit_pager1',{edit:false,add:false,del:false},
			{},	{},	{},{multipleSearch:false,sopt:['cn','nc']});
}
function GetFileComun()
{
	//url='osbb_request_file/';
	tp = $("select#fltypecomun").val();
	enc = $("select#typeenccomun").val();
	//cmd='1?tp='+tp+'&enc='+enc;
	//url+=cmd;
	url=osbb_request_url('file','1','tp='+tp+'&enc='+enc);
	location.href=url;
}
function GetRepFile()
{
	//url='osbb_request_file/';
	tp = $("select#fltype").val();
	enc = $("select#typeenc").val();
	//cmd='2?tp='+tp+'&enc='+enc;
	//url+=cmd;
	url=osbb_request_url('file','2','tp='+tp+'&enc='+enc);
	location.href=url;
}
function GetDebCredReport()
{	
	BuildReportGrid();
	$('#gtfile_box').show();
}
/*
function ShowConfig()
{
	alert($("#comun_table").jqGridExport({exptype:"xmlstring" }));
}*/
