$(document).ready(function(){
	jQuery('#comuntabs').tabs();
	GetPayDays();
	OnColumnHover();
	$('#ccmb').dialog({ autoOpen: false, 
		modal: true,
		buttons: {
		Ok: function() {
			$(this).dialog( "close" );
		}
	}});
	$('div:has(#ccmb)').css('font-size','14px');
});

function OnColumnHover()
{
	$('.payment_table .apart_column').hover(
			function(){
				sName = $(this).attr('name');
				$('th[name2='+sName+']').css("background-color","yellow");
				},
			function(){
				sName = $(this).attr('name');
				$('th[name2='+sName+']').css("background-color","#E7ECFD");
				});
	$('.payment_table .apart_column').click(function(){
		
		sName = $(this).attr('name');
		oTH = $('table tr th[linkname='+sName+']');
		
		aTemp = sName.split("_");
		sApartNum=aTemp[1];
		
		var bSwitch = false;
		var sDay;
		sDay = $(this).attr('day');
		if(sDay!=undefined)
		{
			if(sDay!=''){bSwitch=true;}
		}
		
		if(bSwitch)
		{
			sID = $(this).attr('id');
			aID = sID.split('_');
			sYear = aID[0];
			$sMonth = aID[1];
		}
		
		$('#ccmb_debt').html(oTH.attr('debt'));
		$('#ccmb_kredit').html(oTH.attr('kredit'));
		$('#ccmb_balance').html(oTH.attr('balance'));
		$('#ccmb_pay').html(oTH.attr('pay'));
		if(bSwitch)
		{
			$('#ccmb_payday').html(sDay+'.'+$sMonth+'.'+sYear);
		}
		else
		{
			$('#ccmb_payday').html('-');
		}
		//$('#ccmb_header').html(oTH.attr('name'));
		$('#ccmb_header').html(sApartNum);
		$('#ccmb').dialog('open');
	});
}

function GetPayDays()
{
	paydayurl=osbb_request_url('json','7');//'osbb_request_json/7';
	
	$.getJSON(paydayurl,function(data){
			if(data.ErrorMessage)
			{
				alert(data.ErrorMessage);
			}
			else
			{
				for(i=0;i<data.PayDay.length;i++)
				{
					aData = data.PayDay[i].split(":"); 
					sID = aData[0];
					sDate = aData[1];
					sTag = "<img class='paydayimage' src='"+PayLineImage+"' />";
					$("#"+sID).html(sTag);
					$("#"+sID).attr('day',sDate);
				}
			}
	});
}

function PayTableHeader(e)
{
	$('#ccmb_debt').html($(e).attr('debt'));
	$('#ccmb_kredit').html($(e).attr('kredit'));
	$('#ccmb_balance').html($(e).attr('balance'));
	$('#ccmb_pay').html($(e).attr('pay'));
	$('#ccmb_header').html($(e).attr('name'));
	$('#ccmb').dialog('open');
}