
function AddComunPay()
{
	addurl=osbb_request_url('json','4');//'osbb_request_json/4';
	var tpremise=$.trim($("#apart_n").val());
	var tsum=$.trim($("#pay_amaunt").val());
	var tgrp = $("select#commers_type").val();
	var tismonth = false;
	if($("#pay_ismonth").is(':checked'))
	{ tifmonth = true;	}
	
	if(tpremise=='')
	{
		alert(Message6);
		return false;
	}
	
	if(tsum=='')
	{
		alert(Message7);
		return false;
	}
	
	dtPayOnDate =$("#PayOnDate").datepicker( "getDate" );
	sPayOnDate = dtPayOnDate.getFullYear()+'-'+String(Number(dtPayOnDate.getMonth()+1))+'-'+dtPayOnDate.getDate();
	
	$.getJSON(addurl,{prem:tpremise,summa:tsum,groupid:tgrp,ismonth:tifmonth,ondate:sPayOnDate},function(data){
			if(data.ErrorMessage)
			{
				alert(data.ErrorMessage);
			}
			else
			{
				alert(data.Result);
				$("#apart_n").val('');
				$("#pay_amaunt").val('')
			}
	});
}
function BuildReportGrid()
{
	/*
	dtFrom =$("#datepickerFrom").datepicker( "getDate" );
	dtTo =$("#datepickerTo").datepicker( "getDate" );
	
	sFrom = dtFrom.getFullYear()+'-'+String(Number(dtFrom.getMonth()+1))+'-'+dtFrom.getDate();
	sTo = dtTo.getFullYear()+'-'+String(Number(dtTo.getMonth()+1))+'-'+dtTo.getDate();*/
	url_params = osbb_date_url_params("datepickerFrom","datepickerTo");
	acturl=osbb_request_url('json','1',url_params);//"osbb_request_json/1?dtFrom="+sFrom+"&dtTo="+sTo;
	actediturl = osbb_request_url('json','3',url_params);//"osbb_request_json/3?dtFrom="+sFrom+"&dtTo="+sTo;
	
	if(isLoaded)
	{
		$("#credit_table").jqGrid('clearGridData');
		
		$("#credit_table").setGridParam({url:acturl}); 
		$("#credit_table").trigger("reloadGrid");
		return;
	}
	
	acseturl=osbb_request_url('xml','3',url_params);//"osbb_request_xml/3?dtFrom="+sFrom+"&dtTo="+sTo;
	$("#credit_table").jqGrid('jqGridImport',{impurl:acseturl,importComplete:SetUpCreditTable});
}

function GetDonateReport()
{/*
	dtFrom =$("#dtpDonateFrom").datepicker( "getDate" );
	dtTo =$("#dtpDonateTo").datepicker( "getDate" );
	
	sFrom = dtFrom.getFullYear()+'-'+String(Number(dtFrom.getMonth()+1))+'-'+dtFrom.getDate();
	sTo = dtTo.getFullYear()+'-'+String(Number(dtTo.getMonth()+1))+'-'+dtTo.getDate();*/
	url_params = osbb_date_url_params("dtpDonateFrom","dtpDonateTo");
	dturl=osbb_request_url('json','5',url_params);//"osbb_request_json/5?dtFrom="+sFrom+"&dtTo="+sTo;
	dtediturl = osbb_request_url('json','6',url_params);//"osbb_request_json/6?dtFrom="+sFrom+"&dtTo="+sTo;
	
	if(isDonateLoaded)
	{
		$("#donation_table").jqGrid('clearGridData');
		
		$("#donation_table").setGridParam({url:dturl}); 
		$("#donation_table").trigger("reloadGrid");
		return;
	}
	
	dtseturl=acseturl=osbb_request_url('xml','4',url_params);//"osbb_request_xml/4?dtFrom="+sFrom+"&dtTo="+sTo;
	$("#donation_table").jqGrid('jqGridImport',{impurl:dtseturl,importComplete:SetUpDonateTable});
}
function SetUpCreditTable()
{
	isLoaded = true;
	
	jQuery("#credit_table").jqGrid('navGrid','#credit_pager1',{edit:true,add:true,del:true},
			{width:390,savekey: [true,13], navkeys: [true,38,40], closeAfterEdit:true,reloadAfterSubmit:true, closeOnEscape:true, bottominfo:Message1},	
			{width:390,savekey: [true,13], navkeys: [true,38,40], closeAfterAdd:true,reloadAfterSubmit:true, closeOnEscape:true, bottominfo:Message1},
			{},{multipleSearch:false,sopt:['cn','nc']});
}
function SetUpDonateTable()
{
	isDonateLoaded=true;
	jQuery("#donation_table").jqGrid('navGrid','#donation_pager',{edit:false,add:false,del:true},
			{},{},{},{multipleSearch:false,sopt:['cn','nc']});
}