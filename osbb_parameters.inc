<?php
function osbb_communal_payday()
{
    $oRes = new stdClass();
    $sSql='SELECT sett_value FROM {osbb_settings} where sett_name like "comunal_periodic_id"';
    $sPeriodicID = db_result(db_query($sSql));
    if(isset($sPeriodicID) && !empty($sPeriodicID))
    {
        $sSql='select * from {osbb_payments} y, {osbb_premise} r where y.premiseid = r.premiseid and periodicpayid='.$sPeriodicID;
        $oResult = db_query($sSql);
        $aPayDay = array();
        while ($oDay=db_fetch_object($oResult)) {
            $aDt = getdate(strtotime($oDay->date_oper));
            $sYear = $aDt['year'];
            $sMonth = $aDt['mon'];
            $sDay=$aDt['mday'];
            $sID = $sYear.'_'.$sMonth.'_'.$oDay->premisegroupid.'_'.$oDay->number.':'.$sDay;
            $aPayDay[] = $sID;
        }
        $oRes->PayDay =$aPayDay;
    }
    return $oRes;
}
function osbb_save_settings($sPayOnMonth,$iPeriodicGroup,$sStartViewCredit)
{
    $oRes = new stdClass();
    //Для змінної за 1 кв.м.
    $sSql = osbb_set_value('osbbpayon',$sPayOnMonth);
    db_query($sSql);
    
    //Для змінної періодичного платежу
    $sSql = osbb_set_value('comunal_periodic_id',$iPeriodicGroup);
    db_query($sSql);
    
    //Для початок перыоду для перегляду
    $sSql = osbb_set_value('credit_start_view_period',$sStartViewCredit);
    db_query($sSql);
    
    $oRes->Result=osbb_print_label(79);
    return $oRes;
}

function osbb_save_periodic($sOperation, $sID=0 ,$sRawPeriodicName)
{
    $sRawPeriodicName = trim($sRawPeriodicName);
    if(is_null($sRawPeriodicName) ||empty($sRawPeriodicName))
        return;
    
    $sName = mysql_real_escape_string($sRawPeriodicName);
    $sql='select 1=1';
    switch ($sOperation) {
        case 'edit':
    	    $sql="update {osbb_periodic_pays} set `pay_name`='$sName' where periodicpayid=$sID";
    	break;
    	case 'add':
    	    $sql="insert into {osbb_periodic_pays} (`pay_name`) values ('$sName')";
    	break;
    	case 'del':
    	    $sql="delete from {osbb_periodic_pays} where periodicpayid=$sID";
    	    break;
    }
    db_query($sql);
}
function osbb_save_owner($sOperation, $sID=0 ,$sRawFirstName,$sRawLastName,$sRawMiddleName,$sRawEmail,$sRawPhone1,$sRawPhone2)
{
    $sRawFirstName = trim($sRawFirstName);
    if(is_null($sRawFirstName) ||empty($sRawFirstName))
        return;
    
    $sFirstName = mysql_real_escape_string($sRawFirstName);
    $sLastName = mysql_real_escape_string($sRawLastName);
    $sMiddleName = mysql_real_escape_string($sRawMiddleName);
    $sEmail = mysql_real_escape_string($sRawEmail);
    $sPhone1 = mysql_real_escape_string($sRawPhone1);
    $sPhone2 = mysql_real_escape_string($sRawPhone2);
 
    $sql='select 1=1';
    switch ($sOperation) {
        case 'edit':
    	    $sql="update {osbb_owners} set `first_name`='$sFirstName',`last_name`='$sLastName',`middle_name`='$sMiddleName',`email`='$sEmail',`phone_local`='$sPhone1',`phone_mobile`='$sPhone2'  where ownerid=$sID";
    	break;
    	case 'add':
    	    $sql="insert into {osbb_owners} (`first_name`,`last_name`,`middle_name`,`email`,`phone_local`,`phone_mobile`) values ('$sFirstName','$sLastName','$sMiddleName','$sEmail','$sPhone1','$sPhone2')";
    	break;
    	case 'del':
    	    $sql="delete from {osbb_owners} where ownerid=$sID";
    	    break;
    }
    db_query($sql);
}
function osbb_save_premise($sOperation, $sID=0 ,$iRawOwnerID='0',$iRawSquare,$iRawNumber,$iRawGroup='0',$iRawCorrect='0')
{
    $iRawNumber = trim($iRawNumber);
    if(is_null($iRawNumber) ||empty($iRawNumber))
        return;
        
    $iOwnerId = '0';
    if($iRawOwnerID!='0')
        $iOwnerId = substr($iRawOwnerID, 1, strlen($iRawOwnerID));
    
    $iGroup='0';
    if($iRawGroup!='0')
        $iGroup = substr($iRawGroup, 1, strlen($iRawGroup));
        
    $iCorrect='0';
    if(isset($iRawCorrect) && !empty($iRawCorrect))
        $iCorrect=mysql_real_escape_string($iRawCorrect);
        
    $sql='select 1=1';
    switch ($sOperation) {
        case 'edit':
    	    $sql="update {osbb_premise} set `ownerid`='$iOwnerId',`number`='$iRawNumber',`square`='$iRawSquare',`correction`='$iCorrect',`premisegroupid`='$iGroup'  where premiseid=$sID";
    	break;
    	case 'add':
    	    $sql="insert into {osbb_premise} (`ownerid`,`number`,`square`,`correction`,`premisegroupid`) values ($iOwnerId,$iRawNumber,$iRawSquare,$iCorrect,$iGroup)";
    	break;
    	case 'del':
    	    $sql="delete from {osbb_premise} where premiseid=$sID";
    	    break;
    }
    db_query($sql);
}
function osbb_save_type($sOperation, $sID=0 ,$sRawName)
{
    $sRawName = trim($sRawName);
    if(is_null($sRawName) ||empty($sRawName))
        return;
        
    $sName = mysql_real_escape_string($sRawName);
    $sql='select 1=1';
    switch ($sOperation) {
        case 'edit':
    	    $sql="update {osbb_premise_types} set `type_name`='$sName' where premisetypeid=$sID";
    	break;
    	case 'add':
    	    $sql="insert into {osbb_premise_types} (`type_name`) values ('$sName')";
    	break;
    	case 'del':
    	    $sql="delete from {osbb_premise_types} where premisetypeid=$sID";
    	    break;
    }
    db_query($sql);
}
function osbb_save_group($sOperation, $sID=0, $sRawName, $iRawTypeID='0')
{
    $sRawName = trim($sRawName);
    if(is_null($sRawName) ||empty($sRawName))
        return;
        
    $sName = mysql_real_escape_string($sRawName);
    $sTypeID='0';
    if($iRawTypeID!='0')
        $sTypeID = substr($iRawTypeID, 1, strlen($iRawTypeID));
        
    $sql='select 1=1';
    switch ($sOperation) {
        case 'edit':
    	    $sql="update {osbb_premise_groups } set `group_name`='$sName', `premisetypeid`=$sTypeID where premisegroupid=$sID";
    	break;
    	case 'add':
    	    $sql="insert into {osbb_premise_groups } (`group_name`,`premisetypeid`) values ('$sName',$sTypeID)";
    	break;
    	case 'del':
    	    $sql="delete from {osbb_premise_groups } where premisegroupid=$sID";
    	    break;
    }
    db_query($sql);
}
function osbb_periodic_table($iCurPage=1, $iRowOnPage=10, $sSqlFilter)
{
    $sSqlFilter = str_replace('alias_pay_name','pay_name',$sSqlFilter);
    $sql="SELECT * FROM {osbb_periodic_pays} where $sSqlFilter";
    $oRes = osbb_exec_query_to_object($sql,$iCurPage,$iRowOnPage,'periodicpayid',array('pay_name'));
    
    return $oRes;
}
function osbb_premise_table($iCurPage=1, $iRowOnPage=10, $sSqlFilter)
{
    $sSqlFilter = str_replace('owner_name',"concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.')",$sSqlFilter);
    $sql="SELECT *,concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.') as fio FROM {osbb_premise} p, {osbb_premise_groups} pg, {osbb_owners} o where p.premisegroupid = pg.premisegroupid and o.ownerid = p.ownerid and $sSqlFilter  order by p.premisegroupid, number";
    $oRes = osbb_exec_query_to_object($sql,$iCurPage,$iRowOnPage,'premiseid',array('number','fio','square','group_name','correction'));
    
    return $oRes;
}
function osbb_owners_table($iCurPage=1, $iRowOnPage=10, $sSqlFilter)
{
    $sql="SELECT * FROM {osbb_owners} where $sSqlFilter order by `first_name`,`last_name`";
    $oRes = osbb_exec_query_to_object($sql,$iCurPage,$iRowOnPage,'ownerid',array('first_name','last_name','middle_name','email','phone_local','phone_mobile'));
    
    return $oRes;
}
function osbb_types_table($iCurPage=1, $iRowOnPage=10, $sSqlFilter)
{
    $sql='SELECT * FROM {osbb_premise_types} where '.$sSqlFilter;
    $oRes = osbb_exec_query_to_object($sql,$iCurPage,$iRowOnPage,'premisetypeid',array('type_name'));
    return $oRes;
}
function osbb_group_table($iCurPage=1, $iRowOnPage=10, $sSqlFilter)
{
    $sql = 'SELECT * FROM {osbb_premise_groups} left outer join {osbb_premise_types} on {osbb_premise_groups}.premisetypeid = {osbb_premise_types}.premisetypeid where '.$sSqlFilter;
    $oRes = osbb_exec_query_to_object($sql,$iCurPage,$iRowOnPage,'premisegroupid',array('group_name','type_name'));
    return $oRes;
}
