<?php
function osbb_save_donate($sOper,$sID=0,$p_sData,$fSum,$sReason,$sNote, $sPeriodicID='0', $sUser='')
{
    $sSql="select sett_value from {`osbb_settings`} where sett_name = 'osbbpayon'";
    $dPayOn = db_result(db_query($sSql));
    $sSql="select sum/square*$dPayOn as month_payed, opa.premiseid as premiseid,comunalhelperid 
	from {osbb_payments} opa, {osbb_premise} opr, {osbb_communal_helper} och 
	where opa.premiseid=opr.premiseid and opa.premiseid=och.premiseid";
    $result = db_query($sSql);
    $oRow = db_fetch_array($result);
    $dMonthPayed = $oRow['month_payed'];
    $iPremiseID=$oRow['premiseid'];
    $iHelperID=$oRow['comunalhelperid'];
    switch ($sOper) {
    	case 'edit':
    	    
    	break;
    	case 'add':
    	    
    	    break;
    	case 'del':
    	    $sSql="update {`osbb_communal_helper`} set paid_month=paid_month - $dMonthPayed where comunalhelperid=$iHelperID;";
    	    db_query($sSql);
    	    $sSql="update {osbb_payments} set is_deleted=1,`user`='$sUser' where procid=$sID";
    	    break;
    }
    db_query($sSql);
}
function osbb_save_credit($sOper,$sID=0,$p_sData,$fSum,$sReason,$sNote, $sPeriodicID='0', $sUser='')
{
    $sType='0';
    $sSql='select 1=1';
    $sPID='0';
    if($sPeriodicID!='0')
        $sPID = substr($sPeriodicID, 1, strlen($sPeriodicID));
    $sdt=date('Y-m-d',strtotime($p_sData));
    $sdtUpdate=date('Y-m-d');
    switch ($sOper) {
    	case 'edit':
    	    $sSql="update {osbb_payments} set `sum`=$fSum, `db_or_kr`=$sType, `reason`='$sReason',`date_oper`='$sdt', `note`='$sNote', `periodicpayid`=$sPID, `user`='$sUser', `date_update`='$sdtUpdate' where procid=$sID";
    	break;
    	case 'add':
    	    $sSql = "insert into {osbb_payments} (`sum`,`db_or_kr`,`reason`,`date_oper`,`note`,`user`,`is_deleted`,`periodicpayid`,`premiseid`,`date_update`) values ($fSum,$sType,'$sReason','$sdt','$sNote','$sUser',0,$sPID,0,'$sdtUpdate')";
    	    break;
    	case 'del':
    	    $sSql="update {osbb_payments} set is_deleted=1,`user`='$sUser',`date_update`='$sdtUpdate' where procid=$sID";
    	    break;
    }
    db_query($sSql);
}
function osbb_accounting_payments($sdtFrom, $sdtTo, $Sort, $sWay, $iCurPage, $iRowOnPage=10, $sFilters)
{
    $aRes = array();
    if(empty($Sort))
        $innSort = 'date_oper';
    else
        $innSort=$Sort;
    $sFilters = str_replace('periodicpayid','opp.periodicpayid',$sFilters);//Замынимо назву стовбця на розширену назву
    $where = $sFilters;
    
    $sSql= "SELECT op.*, ifnull(opp.pay_name,'') as pay_name FROM {osbb_payments} op left outer join {osbb_periodic_pays} opp 
	on op.periodicpayid=opp.periodicpayid 
	where $where 
	and op.`date_oper`>='$sdtFrom' 
	and op.`date_oper`<='$sdtTo' 
	and op.is_deleted=0 and db_or_kr=0
	order by `$innSort` $sWay";
    $_SESSION['debcred_report']=$sSql;
    $iCounterRows=0;
    $iTotalSum=0;
    $result = db_query($sSql);
     while ($oRow=db_fetch_array($result)) {
         $aRes[$iCounterRows] = $oRow;
         $iCounterRows++;
         $iTotalSum += $oRow['sum'];
    }
    
    $Obj = new stdClass();
    $Obj->page=$iCurPage;
    $Obj->total=round($iCounterRows/$iRowOnPage,0)+1;
    $Obj->records=$iCounterRows;
    $oUserData = new stdClass();
    $oUserData->totalsum=$iTotalSum;
    $Obj->userdata= $oUserData;
    $ComRes=array();
    $i=0;
    $start = $iRowOnPage*$iCurPage - $iRowOnPage;
    $end=($start+$iRowOnPage+1>$iCounterRows?$iCounterRows:$start+$iRowOnPage+1);
    for ($index = $start; $index < $end; $index++) {
        $ComRes[$i]['id']=$aRes[$index]['procid'];
        $ComRes[$i]['cell']=array($aRes[$index]['procid'],$aRes[$index]['sum'],
                                    $aRes[$index]['date_oper'],$aRes[$index]['reason'],
                                    $aRes[$index]['note'],$aRes[$index]['pay_name']);
        $i++;
    }
    
    $Obj->rows = $ComRes;
    
    return $Obj;
}
function osbb_accounting_communal($sdtFrom, $sdtTo, $Sort, $sWay, $iCurPage, $iRowOnPage=32, $is_moder=false)
{
    $aRes = array();
    if(empty($Sort))
        $innSort = 'apart';
    else
        $innSort=$Sort;
        
    $iStartMonth = $sdtFrom.'-01';
    $iRange=$sdtTo.'-01';
    
    $GlobalStartDate = osbb_start_date(1);
    $iFrom = osbb_diff_in_month($GlobalStartDate,$iStartMonth)-1;
    $iLength = osbb_diff_in_month($iStartMonth,$iRange);
    $iCounterRows=0;
    $iTotalPaidSum=0;
    $iTotalCreditSum=0;
    $iTotalDebetSum=0;
    $sql = "SELECT @iStart:=$iFrom, @iLength:=$iLength, 
		premiseid,apartment,p.premisegroupid, square, balance,correct,	`type1`,short_fio, 
		@mp:=if((`month`-@iStart)<0,0,(`month`-@iStart)) as `mp`, 
		@mpaid:=if((@mp+0)>@iLength+0,@iLength+0,@mp+0) as `mpaid`, 
		@mcred:=if((@iLength-@mp)>0,@iLength-@mp,0) as `mcred`, 
		@mdebet:=if((@iLength-@mp)<0,@mp-@iLength,0) as `mdebet` 
		FROM (select po.*, 
				(ifnull(och.paid_month,0)) as `month`, 
				(ifnull(och.balance,0)) as `balance` 
				from (select p.premiseid, o.ownerid,pg.premisegroupid, p.number as apartment, 
						p.square, p.correction as correct, group_name as type1, 
						concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.') as short_fio 
						from {osbb_premise} p, {osbb_owners} o, {osbb_premise_groups} pg 
						where p.ownerid=o.ownerid and p.premisegroupid=pg.premisegroupid) po 
				left outer join {osbb_communal_helper} och on po.premiseid= och.premiseid) as p 
		order by premisegroupid,apartment";
    $_SESSION['comun_report']=$sql;
    $result = db_query($sql);
    while ($oRow=db_fetch_array($result)) {
        $oCalulated = osbb_calc_summ($oRow);
        $ppaid = $oCalulated->ppaid;
        $pdebet = $oCalulated->pdebt;
        $pcred = $oCalulated->pcredit;
        $aRes[$iCounterRows]=array_merge($oRow,array('ppaid'=>$ppaid,'pcred'=>$pcred,'pdebet'=>$pdebet));
        $iTotalPaidSum += $ppaid;
        $iTotalCreditSum += $pcred;
        $iTotalDebetSum += $pdebet;
        $iCounterRows++;
    }
    $Obj = new stdClass();
    $Obj->page=$iCurPage;
    $Obj->total=round($iCounterRows/$iRowOnPage,0)+1;
    $Obj->records=$iCounterRows;
    $oUserData = new stdClass();
    $oUserData->alltotal=$iTotalPaidSum+$iTotalDebetSum;
    $oUserData->totalsum=$iTotalPaidSum;//$iFrom.':::::'.$iLength.'::::'.$iTotalPaidSum;
    $oUserData->totalcredit=$iTotalCreditSum;
    $oUserData->totaldebet=$iTotalDebetSum;
    $Obj->userdata= $oUserData;
    $ComRes=array();
    $i=0;
    $start = $iRowOnPage*$iCurPage - $iRowOnPage;
    $end=($start+$iRowOnPage+1>$iCounterRows?$iCounterRows:$start+$iRowOnPage+1);
    for ($index = $start; $index < $end; $index++) {
        $ComRes[$i]['id']=$aRes[$index]['paymentid'];
        $ComRes[$i]['cell']=array($aRes[$index]['paymentid'],$aRes[$index]['apartment'],($is_moder?str_replace("'",'`',$aRes[$index]['short_fio']):osbb_print_label(85)),
                                    str_replace("'",'`',$aRes[$index]['type1']),
                                    $aRes[$index]['mpaid'],$aRes[$index]['ppaid'],
                                    $aRes[$index]['mcred'],$aRes[$index]['pcred'],
                                    $aRes[$index]['mdebet'],$aRes[$index]['pdebet']);
        $i++;
    }
    $Obj->rows = $ComRes;
    
    return $Obj;
}
function osbb_accounting_donate($sdtFrom, $sdtTo, $Sort, $sWay, $iCurPage, $iRowOnPage=10, $sFilters)
{
    if(empty($Sort))
        $innSort = 'date_oper';
    else
        $innSort=$Sort;
    $aRes = array();
    $sFilters = str_replace('fio',"concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.')",$sFilters);//Стовбця fio в базы немає, замінимо на вираз
    $where = $sFilters;
    $sSql="select *,
	concat(first_name,' ',left(last_name,1),'.',left(middle_name,1),'.') as fio
 from {osbb_payments} opa, {osbb_premise} opr, {osbb_owners} o where $where and
	periodicpayid in (select sett_value from {osbb_settings} where sett_name='comunal_periodic_id')
	and opa.premiseid=opr.premiseid and opr.ownerid=o.ownerid and is_deleted=0 order by `$innSort` $sWay";
    $iCounterRows=0;
    $iTotalSum=0;
    $result = db_query($sSql);
     while ($oRow=db_fetch_array($result)) {
         $aRes[$iCounterRows] = $oRow;
         $iCounterRows++;
         $iTotalSum += $oRow['sum'];
    }
    
    $Obj = new stdClass();
    $Obj->page=$iCurPage;
    $Obj->total=round($iCounterRows/$iRowOnPage,0)+1;
    $Obj->records=$iCounterRows;
    $oUserData = new stdClass();
    $oUserData->totalsum=$iTotalSum;
    $Obj->userdata= $oUserData;
    $ComRes=array();
    $i=0;
    $start = $iRowOnPage*$iCurPage - $iRowOnPage;
    $end=($start+$iRowOnPage+1>$iCounterRows?$iCounterRows:$start+$iRowOnPage+1);
    for ($index = $start; $index < $end; $index++) {
        $ComRes[$i]['id']=$aRes[$index]['procid'];
        $ComRes[$i]['cell']=array($aRes[$index]['procid'],$aRes[$index]['fio'],
                                   $aRes[$index]['sum'], $aRes[$index]['date_oper']);
        $i++;
    }
    
    $Obj->rows = $ComRes;
    
    return $Obj;
}
function osbb_save_communal_pay($p_sPremiseNum,$p_sSumma,$p_iPremiseGroupID, $p_sIsMonth,$p_sOnDate,$p_sUserName)
{
    $oResult = new stdClass();
    $sSql='SELECT sett_value FROM {osbb_settings} where sett_name like "comunal_periodic_id"';
    $sPeriodicID = db_result(db_query($sSql));
    //Перевірка, чи вказаний тип періодичного платежу для комунальних. Так як потім ми не зможемо відрізнити їх від іншого приходу
    if(isset($sPeriodicID) && !empty($sPeriodicID))
    {
        $sSql="select * from {osbb_premise} where number=$p_sPremiseNum and premisegroupid=$p_iPremiseGroupID";
        $oRes=db_query($sSql);
        $aRes = db_fetch_array($oRes);
        $iPremiseID =  $aRes['premiseid'];
        //Перевірка, чи є така квартира у такій групі
        if(isset($iPremiseID) && !empty($iPremiseID))
        {
            $iSquare=$aRes['square'];//Беремо площу приміщення
            $dSumma = $p_sSumma;
            $dSinglePay = osbb_get_value('osbbpayon');
            $iPaidMonth = floor($dSumma/$dSinglePay);
            $dBalanse = $dSumma-$iPaidMonth;//Залишок
            if($p_sIsMonth=='true')//Якщо вибрано тип оплати - місяць
            {
                $dSumma = $iSquare*$dSinglePay*$p_sSumma;//Вираховуємо сумму, щоб записати у таблицю платежів
                $iPaidMonth = $p_sSumma;
                $dBalanse =0;
            }
            
            $sdtUpdate=date('Y-m-d');
            $sSql = "insert into {osbb_payments} (`sum`,`db_or_kr`,`date_oper`,`user`,`is_deleted`,`periodicpayid`,`premiseid`,`date_update`) values ($dSumma,1,'$p_sOnDate','$p_sUserName',0,$sPeriodicID,$iPremiseID,'$sdtUpdate')";
            db_query($sSql);
            
            //Обробляємо допоміжну таблицю
            $sSql="select * from {osbb_communal_helper} where premiseid=$iPremiseID";
            $oRes=db_query($sSql);
            $aRes = db_fetch_array($oRes);
            $iComunalHelperID = $aRes['comunalhelperid'];
            //Перевірка, чи є в допоміжній таблиці запис про цю квартиру
            if(isset($iComunalHelperID) && !empty($iComunalHelperID))
            {
                $iPaid = $aRes['paid_month'];
                $iPaid += $iPaidMonth;
                $dOldBalanse = $aRes['balance'];
                $dNewBalanse = $dBalanse+$dOldBalanse;
                if($dNewBalanse>$iSquare*$dSinglePay)
                {
                    $AddMonth = floor($dNewBalanse/($iSquare*$dSinglePay));
                    $dNewBalanse = $dNewBalanse - $AddMonth;
                    $iPaid += $AddMonth;
                }
                $sSql = "update {osbb_communal_helper} set paid_month=$iPaid, balance=$dNewBalanse  where comunalhelperid=$iComunalHelperID";
            }
            else 
            {
                $sSql = "insert into {osbb_communal_helper} (paid_month, balance,premiseid) values ($iPaidMonth,$dBalanse,$iPremiseID)";
            }
            
            db_query($sSql);
            
            //Виставляємо останню дату оновлення для групи
            $sDateUpd = date('Y-m-d');
            $sSql="update {osbb_premise_groups} set `update`='$sDateUpd' where premisegroupid=$p_iPremiseGroupID";
            db_query($sSql);
            $oResult->Result=osbb_print_label(94);
        }
        else
            $oResult->ErrorMessage=osbb_print_label(63);
    }
    else
        $oResult->ErrorMessage=osbb_print_label(64);
    return $oResult;
}