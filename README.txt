$Id$

Schema module

PREREQUISITES

Drupal 6.x

OVERVIEW



INSTALLATION

Install and activate Schema like every other Drupal module.

ADMINISTRATOR USAGE

Visit Administer >> Site building >> OSBB settings to access settings UI
functionality.

AUTHOR

Laba Nik
